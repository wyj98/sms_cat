#include "get_time.h"
void get_time(char *now_time)
{
    char                temp[64] ;
    time_t              tim ;
    struct  tm          *timep;
                        
    memset(temp, 0, sizeof(temp)) ;
    time(&tim);
    timep = localtime(&tim);
    snprintf(temp, sizeof(temp), "%d-%d-%d  %d:%d:%d"
    ,(1900+ timep->tm_year), (1+ timep->tm_mon), timep->tm_mday, (timep->tm_hour), timep->tm_min,timep->tm_sec);
    strcpy(now_time, temp) ;
}
