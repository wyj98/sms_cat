/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  get_time.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(2019年07月31日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月31日 20时39分17秒"
 *                 
 ********************************************************************************/

#ifndef _GET_TIME_H
#define _GET_TIME_H

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>


void get_time(char *now_time) ;
#endif

