/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  get_apn.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(2019年07月29日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月29日 16时54分56秒"
 *                 
 ********************************************************************************/
#ifndef     _GET_APN_H
#define     _GET_APN_H

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "iniparser.h"
#include "dictionary.h"

#define DEFAULT_APN_CONF                "default_apn.conf"

#define APN_LEN                         128
#define UID_LEN                         64
#define PWD_LEN                         64


#define APN_3G                          3 
#define APN_2G                          2


typedef struct apn_account_s
{
    char            apn[APN_LEN];
    char            uid[UID_LEN];
    char            pwd[PWD_LEN];
    char            auth[10];  /*   PAP or CHAP */
    // unsigned char   valid;   /*   This APN can use or not */
}apn_account_t;     /*   ----------  end of struct apn_account_s  ---------- */

int get_apn_conf(char *ini_path, char *section, apn_account_t *apn, unsigned char type) ;

#endif
