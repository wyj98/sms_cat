/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  serial.h
 *    Description:  This head file use for com port communication 
 *
 *        Version:  1.0.0(2019年06月04日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年06月04日 18时18分14秒"
 *                 
 ********************************************************************************/
#ifndef  _COMPORT_H
#define  _COMPORT_H
 
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <getopt.h>
#include  <fcntl.h>
#include  <errno.h>
#include  <termios.h>
#include  <sys/stat.h>
#include  <sys/wait.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <sys/select.h>
 
#define BUF_LEN  64
 
#ifndef DEVNAME_LEN
#define DEVNAME_LEN          64
#endif

#define ENABLE_CRTSCTS      1
#define DISABLE_CRTSCTS     0
#define TIMEOUT         -4

 
typedef struct _st_comport
{
    int             databit ;
    char            parity ; 
    int             stopbit ;
    char            flowctrl ; /* ENABLE_CRTSCTS or DISABLE_CRTSCTS */
    char            dev_name[DEVNAME_LEN];
    unsigned char   used;     /*  This comport used or not now */
    int             fd;
    long            baudrate ;
    int             frag_size ;
    struct termios  original_opt ;
} st_comport;


/*  初始化串口结构体 */
st_comport *comport_init(const char *dev_name, long baudrate, int databit, const char parity, int stopbit,const char flowctrl) ;
/*  清理串口结构体 */
void comport_term(st_comport * comport) ;
/*    打开串口     */
int open_comport(st_comport * comport) ;
/*  关闭串口 */
void comport_close(st_comport * comport) ;
/*  设置波特率 */
void set_baudrate (struct termios *opt, int baudrate) ;
int _set_baudrate(struct termios *term,speed_t baudrate) ;
/*  设置停止位 */
void set_stopbit (struct termios *opt, int stopbit) ;
/*  设置数据位 */
void set_data_bit (struct termios *opt, int databit) ;
/*  设置流控        */
void set_flowctrl(struct termios *opt,const char flowctrl) ;
/*   设置校验位    */
void set_parity (struct termios *opt,const char parity) ;
/*  写串口 */
int write_comport(st_comport *comport,char *buf, int buf_size) ;
/* 读串口 */
int read_comport(st_comport *comport, char *buf, int buf_size, int timeout) ;
/* 读gprs模块 */
int read_gsm(st_comport *comport, char *rbuf,int buf_size) ;

#endif
