/*********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  get_apn.c
 *    Description:  This file according to MCC and MSN to get apn username and passwd
 *                 
 *        Version:  1.0.0(2019年07月28日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月28日 17时20分19秒"
 *                 
 ********************************************************************************/
#include "get_apn.h"

int get_apn_conf(char *ini_path, char *section, apn_account_t *apn, unsigned char type)
{
    dictionary          *ini = NULL;
    char                key[64] ;
    const char          *ptr = NULL ;



    if( ini_path == NULL || section == NULL || apn == NULL)
    {
        printf("Invail parameter in %s\n",__FUNCTION__) ;
        return -1 ;
    }

    ini = iniparser_load(ini_path); //Load ini file get mqtt conf 
    if ( ini == NULL ) {
        printf("iniparser_load error!\n");
        return -2;
    }
    printf("Load ini file %s sucessful\n", ini_path) ;

    /*  Get Operators APN from ini  */
    memset(key, 0, sizeof(key)) ;
    snprintf(key, sizeof(key), "%s:%s", section, ( APN_3G == type?"3g_apn":"apn" )) ;
    ptr = iniparser_getstring(ini, key, NULL) ;
    if(ptr == NULL)
    {
        printf("Cant Find %s in ini file %s\n", key, ini_path) ;
        return -3 ;
    }
    strncpy(apn->apn, ptr, APN_LEN);


    /*  Get Operators APN username from ini */
    memset(key, 0, sizeof(key)) ;
    snprintf(key, sizeof(key), "%s:%s", section,( APN_3G == type?"3g_uid":"uid" )) ;
    ptr = iniparser_getstring(ini, key, NULL) ;
    strncpy(apn->uid, ptr, UID_LEN);


    /*  Get Operators APN password from ini */
    memset(key, 0, sizeof(key)) ;
    snprintf(key, sizeof(key), "%s:%s", section,( APN_3G == type?"3g_pwd":"pwd" )) ;
    ptr = iniparser_getstring(ini, key, NULL) ;
    strncpy(apn->pwd, ptr, PWD_LEN);


#if 1
    printf("SIM Card APN:\"%s\", User:\"%s\", Passwd:\"%s\"\n",apn->apn, apn->uid, apn->pwd) ;
#endif
    return 0 ;
}
