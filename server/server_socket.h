/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  server_socket.h
 *    Description:  This head file is server_socket.c
 *
 *        Version:  1.0.0(2019年06月07日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年06月07日 15时31分02秒"
 *                 
 ********************************************************************************/
#ifndef _SERVER_SOCKET_
#define _SERVER_SOCKET_

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libgen.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define TRUE        0
#define ERROR       -1
#define BACK_LOG     13



/*********************************************************************************************************
 * 描述：    创建初始化服务器套接字                                                                      *
 * 
 *  参数：    char *lis_addr监听的IP地址字符串"x.x.x.x",如果传NULL则监听所有网卡IP地址htonl(INADDR_ANY) ;*
 *            int port指定监听的端口 .                                                                   *
 *          
 *  返回值：                                                                                             *
 *           成功返回socket文件描述符                                                                    *
 *           失败返回负数
 *********************************************************************************************************/ 
int creat_socket(char *lis_addr, int port); //socket()、bind()、listen()

/***********************************************************************
 * 描述： 接受新的客户端
 *
 *  参数：   int listen_fd传的服务器socket对应的文件描述符
 *
 *  返回值： 
 *          成功返回接收到的客户端文件描述符
 *          失败返回负数.
 * *********************************************************************/
int accept_client(int listen_fd) ; //accept new client*/

#endif
