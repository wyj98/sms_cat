/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  connect_server.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(2019年07月31日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月31日 09时07分59秒"
 *                 
 ********************************************************************************/
#ifndef _CONN_SERV_H
#define _CONN_SERV_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>          /*  See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>



/********************************************************
 *  Describe:client create socket and connect server
 *
 *  Return:
 *          success return fd of client, fail return negative 
 * ****************************************************/
int connect_server(char *ip, int port) ;


#endif /*-------ifndef _CONN_SERV_H-----*/
