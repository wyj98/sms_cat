/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  pdu_sms.h
 *    Description:  This head file PDU sms
 *
 *        Version:  1.0.0(2019年07月30日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月30日 10时46分04秒"
 *                 
 ********************************************************************************/
#ifndef _PDU_SMS_H
#define _PDU_SMS_H


#include <stdio.h>
#include <string.h>
#include <errno.h>


/* ***********************************************************************************************  
 *  Describe:format receiver sms phone number for send pdu sms
 * 
 *  Input parameter: 
 *              const char *center_num is string of center sms phone number
 *              const char *rec_num is string of receiver sms phone number
 *              const char *mess is message content by Unicode ,mess content must be Unicode!!!
 *  Output parameter:
 *              char *out_str is after packet pdu sms 
 *  Return:
 *         OK return send length for AT+CMGS=${Send Length}; Fail return negative
 **************************************************************************************************/
int packet_pdu_sms(const char *center_num, const char *rec_num, const char *mess, char *out_str, int out_size) ;



/* ***********************************************************************
 *  Describe:format receiver sms phone number for send pdu sms
 *
 *  Input parameter: 
 *             const char *rec_num is string of receiver sms phone number
 *  Output parameter:
 *             char *out_str is after format receiver sms phone number output 
 *  Return:
 *     OK return 0; Fail return negative
 ************************************************************************/
int format_receive_num(const char *rec_num,char *out_str, int out_size) ;




/* ***********************************************************************
 *  Describe:format center sms phone number for send pdu sms
 * 
 *  Input parameter: 
 *            const char *center_num is string of center sms phone number
 *  Output parameter:
 *              char *out_str is after format center sms phone number output 
 *  Return:
 *       OK return 0; Fail return negative
 ************************************************************************/
int format_center_num(const char *center_num,char *out_str, int out_size) ;


/* ***********************************************************************************
 *  Describe:fomatt phone_number for send pdu sms , Delete '+', 
 *      if string is odd add 'F', and swap the odd position and even position 
 * 
 *  Input parameter: 
 *               char *phone_num,phone_number string you want to format
 *
 *  Output parameter:
 *                  char *out_str, After fomat phone number string output to out_str
 *  Return :
 *         Successful retrun 0; Fail return negative
 ************************************************************************************/
int format_phone_num(const char *phone_num, char *out_str, int out_size);



/* ****************************************************************************
 *  Describe:swap strings the odd position and even position of the neighbour
 *   
 *  Input parameter:
 *             const char *str, the string you want to swap
 *             int str_len, strlen of the string , it must be even
 *             int out_str, char *str_out size, output string size
 *  Output parameter:
 *             char *str, the string after swap output to char *str_out
 * 
 *  Return:Successful return 0, Fail return negative
 ******************************************************************************/
int swap_odd_even(const char *str,int str_len, char *str_out, int out_size) ;

#endif /*--------ifndef _PDU_SMS_H---------*/


