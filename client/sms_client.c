/*********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  sms_client.c
 *    Description:  This file demo sms cat client 
 *                 
 *        Version:  1.0.0(2019年08月03日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年08月03日 13时43分29秒"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <libgen.h>
#include <getopt.h>
#include <netdb.h>

#include "connect_server.h"
#include "packet_pdu_sms.h"


#ifndef     BUF_SIZE    
#define     BUF_SIZE    512
#endif

int g_stop = 0 ;



void packet_buf(const char* request,char *out_str, int out_size) ;


void sig_handler(int SIG_NUM)
{
    if(SIG_NUM == SIGUSR1)
        g_stop = 1 ;
}

void print_usage(const char *program_name)
{
    printf("\n%s -- (2019.8.3)\n", program_name);
    printf(" Usage: %s -i <server_ip/server_hostname> -p <server_port>  [-h <server_use_help>]\n", program_name);
    printf("        -p --port       the port of the server you want to connect\n") ;
    printf("        -i --ip         the ip address or hostname of the server you want to connect\n") ;
    printf("        -h --help       the client file how to use\n");

    return ;
}

int main(int argc, char **argv)
{
    char                *program_name ;
    int                 port = 0 ;
    int                 opt = -1 ;
    char                *ip = NULL;
    struct hostent *    hostnp ;
    int                 sock_fd = -1;
    struct sigaction    sa;
    char                request[BUF_SIZE] ;
    char                buf[BUF_SIZE] ;
    int                 index = 0 ;


    program_name = basename(argv[0]) ;

    const char *short_opts = "i:n:p:h";                    
    const struct option long_opts[] =   {  
        {"help", no_argument, NULL, 'h'},    
        {"ip", required_argument, NULL, 'i'},
        { "port", required_argument, NULL, 'p'},  
        {0, 0, 0, 0} 
    };  
    while ((opt= getopt_long(argc, argv, short_opts, long_opts,NULL)) != -1) 
    {
        switch (opt) 
        {
            case 'i':
                ip = optarg ;
                break ;
            case 'p':
                port = atoi(optarg) ;
                break ;
            case 'h':
                print_usage(program_name) ;
                return 0 ;
        }
    }
    if( ip == NULL || port == 0 )
    {    
        print_usage(program_name);
        return 0;
    }

    /*  connect server get host by name    */
    if ( inet_addr(ip)== INADDR_NONE )
    {
        if( (hostnp = gethostbyname(ip) ) == NULL )
        {
            printf("get host by name failure: %s\n", strerror(h_errno)) ;
            return -1 ;
        }
        printf("hostname %s\n", hostnp->h_name);
        ip = inet_ntoa( * (struct in_addr *)hostnp->h_addr );
        printf("addr:%s\n",ip) ;
    }


    /*   program stop till get sinal "SIGUSR1"   */
    signal(SIGUSR1, sig_handler);

    /*    server close,client still write to server will  have sigpipe and kill process  */
    sa.sa_handler = SIG_IGN;
    sigaction( SIGPIPE, &sa, 0 );

    while(!g_stop)
    {
        if(sock_fd < 0)
        {
            if( (sock_fd=connect_server(ip, port)) < 0 )
            {
                printf("connect to server failure\n");
            }
            else
            {
                printf("connect suecessfully!\n") ;
            }

        }
        if(sock_fd >= 0)
        {
            printf("\nPlease input:\n") ;
            printf("\t\"rec_sms\" to recevie sms ,\n") ; 
            printf("\t\"send_text\" to send text sms,\n") ; 
            printf("\t\"send_pdu\" to send pdu sms\n") ; 

            memset(request, 0, sizeof(request)) ;
            scanf("%s",request) ;
            packet_buf( request, buf, sizeof(buf)) ;
            if( write(sock_fd, buf, strlen(buf)) < 0 )
            {
                printf("write to server failed: %s\n", strerror(errno)) ;
                close(sock_fd) ;
                sock_fd = -1 ;
                continue ;
            }
            memset(buf, 0, sizeof(buf)) ;
            if( read(sock_fd, buf, sizeof(buf)) < 0 )
            {
                printf("read from server failed: %s\n", strerror(errno)) ;
                close(sock_fd) ;
                sock_fd = -1 ;
                continue ;
            }
            printf("Read from server:%s\n",buf) ;
            if(!strcmp(request,"rec_sms"))
            {
                printf("Please input sms index to read sms:") ;
                scanf("%d", &index) ;
                memset(buf, 0, sizeof(buf)) ;
                snprintf(buf, sizeof(buf),"%d", index) ;
                if( write(sock_fd, buf, strlen(buf)) < 0 )
                {
                    printf("write to server failed: %s\n", strerror(errno)) ;
                    close(sock_fd) ;
                    sock_fd = -1 ;
                    continue ;
                }
                memset(buf, 0, sizeof(buf)) ;
                if( read(sock_fd, buf, sizeof(buf)) < 0 )
                {
                    printf("read from server failed: %s\n", strerror(errno)) ;
                    close(sock_fd) ;
                    sock_fd = -1 ;
                    continue ;
                }
                printf("Read sms content form server:%s\n",buf) ;
            }

        }

    }//end of while(!g_top)


    return 0 ;
}/* End Of Main */


void packet_buf(const char* request,char *out_str, int out_size)
{
    char    rec_phone[BUF_SIZE] ;
    char    center_phone[BUF_SIZE] ;
    char    content[BUF_SIZE] ;
    char    tmp[BUF_SIZE] ;
    int     rv = -1 ;

    if( request == NULL || out_str == NULL || out_size <= 0 )
    {
        printf("Invail input parameter in %s\n", __FUNCTION__) ;
        return  ;
    }
    if( !strcmp(request, "send_text") )
    {
        memset(rec_phone, 0, sizeof(rec_phone)) ;
        memset(content, 0, sizeof(content)) ;
        printf("Please input recevier telephone:") ;
        scanf("%s", rec_phone) ;
        printf("Please input content:") ;
        scanf("%s", content) ;
        snprintf(out_str, out_size, "%s/%s/%s", request, rec_phone, content) ;
    }
    else if( !strcmp(request, "send_pdu") )
    {
        memset(center_phone, 0, sizeof(center_phone)) ;
        memset(rec_phone, 0, sizeof(rec_phone)) ;
        memset(content, 0, sizeof(content)) ;
        printf("Please input center sms phone:") ;
        scanf("%s", center_phone) ;
        printf("Please input recevier telephone:") ;
        scanf("%s", rec_phone) ;
        printf("Please input content:") ;
        scanf("%s", content) ;
        memset(tmp, 0, sizeof(tmp)) ;
        rv = packet_pdu(center_phone, rec_phone, content, tmp, sizeof(tmp)) ;
        if(rv < 0)
        {
            printf("packet_pdu() error\n") ;
            return ;
        }
        snprintf(out_str, out_size, "%s/%d/%s", request, rv, tmp) ;
    }
    else
    {
        strncpy(out_str, request, out_size) ;
    }
}

