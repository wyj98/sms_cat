/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  gsmd.h
 *    Description:  This head file gsmd
 *
 *        Version:  1.0.0(2019年06月30日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年06月30日 13时47分37秒"
 *                 
 ********************************************************************************/
#ifndef     _GSMD_H
#define     _GSMD_H

#include <stdio.h>
#include <pthread.h>
#include "comport.h"
#include "get_apn.h"


/*  $Sim5360E的ttyUSB2和ttyUSB3可用于发送AT命令,此处ttyUSB2用来与模块通信，ttyUSB3用来探测信号实时反馈  */
#define Control_COM     "/dev/ttyUSB3"
#define Data_COM        "/dev/ttyUSB2" 

#define     Running     1
#define     Exit        0

enum{
    OPEN_ERROR = -6 ,
    NOT_IN ,
    NO_SIGNAL ,
    ERROR_IMSI ,
    NO_CREG ,
    NO ,
    OK
} ;

#define     REQ_PPP     (1<<0)
#define     REQ_SMS     (1<<1)
#define     FREE        1
#define     BUSY        0
#define     ON          1
#define     OFF         0


typedef struct _st_worker 
{
    int                         current ; 
    int                         request ; //数字越大优先级越大，0是空闲状态，1是ppp拨号上网，2是发短信
    int                         PPP;//ON/OFF
    pthread_mutex_t             lock ;
    int                         status; //BUSY/FREE
    char                        tel_num[16] ;   //发短信的号码
    char                        mess[512] ; //发短信的短信内容
    int                         pdu_len ;
}st_worker ;

typedef struct _st_gsmRegsiter 
{   
    int             ready ; //如果下面都都没问题设置为YES，否则设置为NO
    char            mcc_buf[4] ;
    char            msn_buf[3] ;
    char            mcc_mnc[32] ;
    int             sim_signal ;//AT+CSQ
    char            operater[32] ;//AT+COPS?
    int             sim_register ;//AT+CREG?
    int             sim_present; //AT+CPIN?
    apn_account_t   simapn ;
}st_gsmRegsiter ;

typedef struct _st_gsmSocket
{
    int         fd ;//服务器文件描述符
    int         cli_fd ;//客户端文件描述符
    int         port ;
    char        *lis_addr ;
}st_gsmSocket ;

typedef struct _st_gsmd_ctx{
    st_comport          *comport ;//串口
    st_gsmRegsiter      gsmRegInfo ;//注册信息
    st_worker           worker ;
    st_gsmSocket        gsmSocket ; //用于进行socket通信 
} st_gsmd_ctx ;





/****************************************************************
 *   描述： 初始化gsmd结构体
 * 
 *   参数：
 *       port服务器用于监听的端口
 *   返回值：
 *       成功返回malloc分配的指向gsmd结构体的指针，失败返回NULL
 * ***************************************************************/
st_gsmd_ctx* init_gprs_module(int port) ;

/************************************
 *  描述： 销毁gsmd结构体
 *
 *************************************/
void destroy_gsmd(st_gsmd_ctx *gsmd_ctx) ;


/********************************************
 *  描述：根据检查模块注册情况的返回值进行休眠
 * 
 *********************************************/
void gsmd_sleep(int reval) ;

/* ******************************************************************
 *  描述： 检查程序是否在运行
 * 
 *  参数： const char *program_name程序名
 *  
 *  返回值：
 *        检查失败返回负数，程序还在运行返回Running, 程序退出了返回Exit
 *************************************************************************/
int check_program_running(const char *program_name) ;



#endif
