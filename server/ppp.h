/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  ppp.h
 *    Description:  This head file ppp conect 
 *
 *        Version:  1.0.0(2019年07月11日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月11日 20时26分58秒"
 *                 
 ********************************************************************************/
#ifndef _PPP_H_
#define _PPP_H_ 


#include "at_cmd.h"
#include "gsmd.h"
#include "comport.h"

#define  Chinese_MCC    460
#define  PPP0_UP        1
#define  PPP0_DOWN      0



/* ***********************
 * 描述：PPP拨号连接
 *
 * 参数：
 *      st_gsmResiter结构体获取MCC值和MSN值
 *      char *chat_conn是用于拨号连接的脚本文件名
 *
 * 返回值：
 *            成功返回0，失败返回负数
 ************************/
int ppp_connect(st_gsmRegsiter gsmRegInfo, const char *pppd_script,const char *conn_script, const char *ppp_dev) ;


/* ************************************
 *   描述： 根据不同运营商设置apn
 * 
 *   返回值：
 *          成功返回0，失败返回负数
 *************************************/
int  set_apn(st_gsmRegsiter gsmRegInfo,const char *pppd_script,const char* conn_script);



/*********************************************** 
 * 描述：断开PPP拨号连接
 * 
 * 参数：
 *      通过哪个AT命令串口设备发送ATH0来断开连接
 * 返回：
 *      成功返回0， 失败返回负数
 ***********************************************/
int close_ppp(const char *name);


/************************** 
 *  描述：测试网络连通性
 * 
 *  返回值：
 *        成功返回0，,失败返回负数
 *******************************************/
int ping_check_net(void) ;

/************************************************************  
 *  描述： 使用ifcongfig相对应的网卡，查看网卡是否使能
 *  
 *******************************************************/
int ifconfig_check_ppp(const char *ppp_dev) ;

/* ******************************************
 * 描述：通过ifup-ppp脚本拨号上网
 *
 * 返回：
 *     成功返回0， 失败返回负
 ******************************************/
int ppp_conn(const char *ifup_ppp_path, apn_account_t *apn, const char *ppp_dev) ;

#endif
