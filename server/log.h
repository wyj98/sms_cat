/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  log.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(2019年07月31日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月31日 10时33分52秒"
 *                 
 ********************************************************************************/
#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

void log_roll_back(int log_fd) ;

#endif /*---#ifndef _LOG_H---*/
