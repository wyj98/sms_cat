/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  at_cmd.h
 *    Description:  This head file at_cmd.c
 *
 *        Version:  1.0.0(2019年07月04日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年07月04日 21时42分40秒"
 *                 
 ********************************************************************************/
#ifndef     _AT_CMD_H
#define     _AT_CMD_H_
#include "comport.h"
#include "gsmd.h"
#include <sys/time.h>

#define     UNREAD  0
#define     ALL     1



/* ****************************************************
 *  描述: 发送 AT+CPIN? 命令到GPRS模块来检查sim卡是否到位
 *
 *  返回值： 
 *          成功返回0  失败返回负数
 **************************************************************/
int atcmd_check_sim_present(st_comport *comport) ;

/* ****************************************************
 *  描述: 发送 AT+CSQ 命令到GPRS模块来检查sim卡信号
 *  
 *  返回值： 
 *        成功返回0  失败返回负数
 **************************************************************/
int atcmd_check_sim_signal(st_comport *comport) ;

/* ****************************************************
 * 描述: 发送 AT+COPS? 命令到GPRS模块来检查sim卡信号
 * 输出值： 
 *       oprator_buf返回运营商名称
 * 返回值： 
 *       成功返回0  失败返回负数
 **************************************************************/
int atcmd_check_sim_operator(st_comport *comport , char *operator_buf, int buf_size) ;


/* ****************************************************
 * 描述: 发送 AT+CSQ 命令到GPRS模块来检查sim卡信号
 *  
 * 输出值：
 *        15位IMSI值以字符串形式返回imsi_buf
 * 返回值：                         
 *        成功返回0  失败返回负数  
 **************************************************************/
int atcmd_check_sim_imsi(st_comport *comport , char *mcc_buf, int mcc_size, char *msn_buf, int msn_size);



/*****************************************************
 * 描述: 发送 AT+CREG? 命令到GPRS模块来检查sim卡网络注册情况
 * 
 * 返回值： 
 *       成功返回0  失败返回负数
 *************************************************************/
int atcmd_check_sim_creg(st_comport *comport) ;



/* ****************************************************
 * 描述: 发送 AT+CPSI? 命令到GPRS模块来获取sim卡MCC与MNC
 * 
 *  返回值： 
 *          成功返回0  失败返回负数
 **************************************************************/
int atcmd_check_sim_location(st_comport *comport, char *mcc_mnc, int size) ;


/***************************************  
 *  描述：发短信
 * 
 *  返回值：
 *      成功返回0, 失败返回负数
 ****************************************/
int send_mess(const char*name,const char *phone_num,const char *sms);



/* ***************************************************************************************
 *  描述：发PDU格式短信短信
 *  参数：AT_inter通过哪个AT接口发送，len实际发送长度,pdu_sms是PDU格式装包之后的发送内容
 *  返回： 
 *      成功返回0， 失败返回负数
 *****************************************************************************************/
int send_pdu_sms(const char *AT_inter, int len, char *pdu_sms) ;



/* ***************************************
 *  描述：检查Sim卡是否注册上 
 * 
 *  参数：const char *name通过哪个AT接口去发送AT命令去检查
 *      st_gsmd_ctx结构体用来赋值注册情况如：信号注册商等
 *  返回值：
 *       需要声明头文件gsmd.h,成功返回OK,失败返回其他 
 **************************************************************/
int check_regsiter(const char *name,st_gsmd_ctx *gsmd_ctx) ;


/********************************************
 *  描述：发送AT命令
 *  参数：st_comport *comport指向串口相关结构体st_comport的指针
 *        char *cmd是需要发送的AT命令
 *        char *buf传入字符串用于接收GPRS模块返回的信息
 *        int buf_size字符串的大小
 *        int timeout 传入超时时间
 *  输出值：
 *        buf返回发送AT命令后对应的GPRS返回的内容
 *  返回值：
 *        成功返回0，失败返回负数 
 **********************************************/
int  send_cmd(st_comport *comport, char *cmd, char *buf, int buf_size, int timeout) ;



/* *******************************************************************************************************
 *  描述：  读取短信
 * 
 *  输入参数：
 *      comport是指向st_comport类型的指针，int index是读取短信的位置，int size是输错参数字符串的大小
 *  
 *  输出参数：
 *      rec_buf用于输出收到的短信内容
 *       
 *  返回值：
 *      成功返回0， 失败返回负数
 *             
 ***************************************************************************************************/
int read_sms(st_comport *comport,int index,char *rec_buf, int rec_size) ;


/****************************************************************************************
 *  描述：  检查未读短信
 *  输入参数：
 *          st_comport结构体类型指针，index用于输出短信位置的数组，index_size数组大小
 *   
 *  输出参数：
 *          int 类型数组index[]，返回短信位置的数组
 *  返回：
 *          成功返回未读短信数，失败返回负数
 ***************************************************************************************/
int check_unread_mess(st_comport *comport, int *index, int index_size) ;


/*********************************************
 *  描述：删除短信
 * 
 *  参数：index 删除短信的位置
 *  
 *  返回值：
 *       成功返回0, 失败返回负数
 ************************************************/
int delete_sms(st_comport *comport ,int index) ;

/* ***************************************************************************************
 *  描述：  2019/7/31,发送AT+CMGL="ALL"或AT+CMGL="REC UNREAD"列出短信
 *  输入参数：
 *      st_comport结构体类型指针，int *index用于输出短信位置的数组，index_size数组大小,
 *      type == UNREAD/ALL 
 *
 *  输出参数：
 *       int 类型数组index[]，返回短信位置的数组
 *  返回：
 *      成功返回短信数，失败返回负数
 ****************************************************************************************/
int check_cmgl(st_comport *comport, int *index, int index_size, int type) ;



#endif
