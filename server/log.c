#include "log.h"

void log_roll_back(int log_fd)
{   
    static  int                 filelen = 0 ;
    struct  stat                f_stat ;
    static  int                 loop = 0 ;

    fstat(log_fd, &f_stat);
    printf("\nlog file size: %ld bytes\n", f_stat.st_size);
    if(filelen == 0)
    {
        if( f_stat.st_size > 1024 ) //record filelen when file size bigger than 1024
        {
            filelen = lseek(log_fd, 0, SEEK_END) ;
        }
    }
    else
    {   
        if(loop == 0)
        {
            lseek(log_fd, 0, SEEK_SET) ;
            loop = 1 ;
            printf("Log Already Loopback\n") ;
        }
        if(filelen <= lseek(log_fd, 0, SEEK_CUR)) //if lseek SEEK_CUR longger than filelen, log file roll back   
        { 
            loop = 0 ;
        }
    }
}
