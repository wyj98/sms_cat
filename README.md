# sms_cat

#### 介绍
使用3G模块sim5360E接收短信，发送TEXT格式短信以及PDU格式短信，实现类似于短信猫一样的功能
    
所谓短信猫，其实是一种用来收发短信的设备，他和我们用的手机一样，需要手机SIM卡的支持，在需要收发短信的时候，在短信猫里面插入一张我们平时用的手机卡，插上电源，通过（USB或者串口、网口）数据线和电脑相连，在电脑的应用管理软件中就可以实现短信收发的功能。
    
我实现的是服务器运行在有3g模块的开发板上，客户端可用网线连接开发板，socket连接发送请求去查看短信，发送短信，如果要发送PDU格式的短信，客户端需要运行在支持中文字符集的环境下zh_CN.utf8.


#### 软件架构
3G模块：SIM5360E
开发板：FL2440
交叉编译器：arm-linux-gcc  【gcc version 4.4.6 (crosstool-NG 1.16.0)】 

项目框图：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0905/105510_f733c474_2171782.png "屏幕截图.png")


#### 安装教程

make

#### 使用说明

1. 服务器端运行在开发板上./sms_cat -p ${port} -d
~ >: ./sms_cat 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/122022_c474a489_2171782.png "屏幕截图.png")
sms_cat -- (2019.7.31)

 Usage: sms_cat -p <server_port>  [-h <server_use_help>]

        -p --port       the server listen port

        -h --help       the server file how to use

        -d --daemon     the server progame running in backgruand
2. 客户端运行在PC上./sms_client -i ${server ip or hostname} -p ${port}
![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/122224_75c8f981_2171782.png "屏幕截图.png")
wyj@wyj-virtual-machine:~/Project/sms_cat/client$ ./sms_client 

sms_client -- (2019.8.3)
 Usage: sms_client -i <server_ip/server_hostname> -p <server_port>  [-h <server_use_help>]

        -p --port       the port of the server you want to connect

        -i --ip         the ip address or hostname of the server you want to connect

        -h --help       the client file how to use


#### 运行效果
查看短信：
客户端输入“rec_sms”并发送,服务器端会回有几条短信可读，分别在哪个位置，接着客户端需要输入要读取的短信位置
![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/122527_3973e0a1_2171782.png "屏幕截图.png")

服务器端

![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/123334_52011789_2171782.png "屏幕截图.png")

发送text格式的短信：
客户端输入“send_text”，接着还需要输入收信对方的号码还有发送的内容，如果发送成功服务器会回复OK
![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/123640_c3ea88bb_2171782.png "屏幕截图.png")

服务器端

![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/124008_29068bec_2171782.png "屏幕截图.png")

发送PDU格式短信：
客户端输入“send_pdu”，接着还需要输入短信中心号码，收信对方手机号，还有发送的内容
![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/124340_9effd123_2171782.png "屏幕截图.png")

服务器端

![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/124610_25742fe6_2171782.png "屏幕截图.png")

手机收到的消息：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0804/124724_f0844c1a_2171782.png "屏幕截图.png")