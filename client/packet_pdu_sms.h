/********************************************************************************
 *      Copyright:  (C) 2019 Wu Yujun<540726307@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  packet_pdu_sms.h
 *    Description:  This head file packet pdu
 *
 *        Version:  1.0.0(2019年08月03日)
 *         Author:  Wu Yujun <540726307@qq.com>
 *      ChangeLog:  1, Release initial version on "2019年08月03日 14时57分06秒"
 *                 
 ********************************************************************************/
#ifndef _PACKET_PDU_H
#define _PACKET_PDU_H


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <wchar.h>
#include <locale.h>

int packet_pdu(const char *center, const char *rec_phone, const char *content, char *out_str, int out_size) ;
int string2unicode(const char*str, char *out, int out_size) ;
int packet_pdu_sms(const char *center_num, const char *rec_num, const char *mess, char *out_str, int out_size) ;
int format_receive_num(const char *rec_num,char *out_str, int out_size) ; 
int format_center_num(const char *center_num,char *out_str, int out_size) ;
int format_phone_num(const char *phone_num, char *out_str, int out_size) ;
int swap_odd_even(const char *str,int str_len, char *str_out, int out_size) ;


#endif /* #ifndef _PACKET_PDU_H */

